import { Component, OnInit, ViewChild } from '@angular/core';
import { LatLngLiteral } from '@agm/core';
import { Polygon } from '@agm/core/services/google-maps-types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  data: Array<Object> = [
    {
      location: [
        { lat: 4.701892, lng: -74.066924 },
        { lat: 4.699956, lng: -74.055453 },
        { lat: 4.706368, lng: -74.054422 },
        { lat: 4.708273, lng: -74.064223 },
        { lat: 4.703495, lng: -74.065225 }
      ],
      configuration:
      [
        {
          polyColor: "#ff0000",
          strPolyColor: "#333333"
        }
      ]
    },
    {
      location: [
        { lat: 4.627843, lng: -74.081201 },
        { lat: 4.632778, lng: -74.084416 },
        { lat: 4.635624, lng: -74.091130 },
        { lat: 4.639729, lng: -74.093985 },
        { lat: 4.645746, lng: -74.085373 },
        { lat: 4.641128, lng: -74.079570 }
      ],
      configuration: [
        {
          polyColor: "#00ff00",
          strPolyColor: "#333333"
        }
      ]
    }
  ]

  polyMouseOver(e){
    console.log(e);
  }

  log(val) { console.log(val); }

  title = 'AGM-Example';
  latitude = 4.638877;
  longitude = -74.093948;
  zoom = 11.5;
  


  onClickPolygon(){

  }

  paths: Array<LatLngLiteral> = [
    { lat: 4.627843, lng: -74.081201 },
    { lat: 4.632778, lng: -74.084416 },
    { lat: 4.635624, lng: -74.091130 },
    { lat: 4.639729, lng: -74.093985 },
    { lat: 4.645746, lng: -74.085373 },
    { lat: 4.641128, lng: -74.079570 }
  ]

  markers = [
	  {
      id: 1,
      markerlatitude: 4.7114196,
      markerlongitude: -74.0334368,
      visible: true
    },
    {
      id: 2,
		  markerlatitude: 4.6490862,
      markerlongitude: -74.0660854,
      visible: true
    },
    {
      id: 3,
      markerlatitude: 4.5978931,
      markerlongitude: -74.0958281,
      visible: false
    } 
  ]

  clickedMarker(e, m){
    e.visible = false;
    console.log(e);
    console.log(m);
  }

  clickedDelete(e){
    console.log(e);
  }
}