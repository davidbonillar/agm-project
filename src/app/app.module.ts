import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponentComponent } from './map-component/map-component.component';

import { AgmCoreModule } from '@agm/core'
@NgModule({
  declarations: [
    AppComponent,
    MapComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDnzWcILNkxhOLF8pR8n4gGyxxE8ZfepF4'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
